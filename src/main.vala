
int main (string[] args) {
    typeof (NewTabView).ensure ();

    var app = new SafariNewTab.Application ();
    return app.run (args);
}
