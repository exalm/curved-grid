public class NewTabView : Gtk.Widget {
    private const string REFLECTION_SHADER = """
uniform sampler2D u_texture1;
uniform float reflectionY;

#define MAX_OPACITY 0.25
#define GRADIENT_HEIGHT 100

void mainImage(out vec4 fragColor,
               in vec2 fragCoord,
               in vec2 resolution,
               in vec2 uv) {
    fragColor = GskTexture(u_texture1, uv);

    if (fragCoord.y <= reflectionY)
        return;

    uv.y = 1 - (2 * reflectionY - fragCoord.y) / resolution.y;
    float opacity = 1 - (fragCoord.y - reflectionY) / GRADIENT_HEIGHT;
    opacity *= opacity;
    opacity *= MAX_OPACITY;

    fragColor += GskTexture(u_texture1, uv) * opacity;
}
""";

    private const double SHRINK_AMOUNT = 0.2;
    private const double SHRINK_CENTER = 0.5;

    private const string CURVATURE_SHADER = """
uniform sampler2D u_texture1;

#define PI 3.1415926
#define SHRINK_AMOUNT 0.2
#define SHRINK_CENTER 0.5

void mainImage(out vec4 fragColor,
               in vec2 fragCoord,
               in vec2 resolution,
               in vec2 uv) {
    uv.y -= sin(uv.x * PI) * SHRINK_AMOUNT * (SHRINK_CENTER - uv.y);

    if (uv.y < 0 || uv.y > 1)
        fragColor = vec4(0, 0, 0, 0);
    else
        fragColor = GskTexture(u_texture1, uv);
}
""";

    private Gtk.FlowBox flowbox;

    private Gsk.GLShader? reflection_shader;
    private bool reflection_shader_compiled;

    private Gsk.GLShader? curvature_shader;
    private bool curvature_shader_compiled;

    private Gtk.Widget hovered_widget;
    private Gtk.Widget pressed_widget;

    construct {
        flowbox = new Gtk.FlowBox () {
            min_children_per_line = 4,
            max_children_per_line = 4,
            margin_top = 100,
            margin_bottom = 100,
            margin_start = 12,
            margin_end = 12,
            selection_mode = NONE,
            can_target = false,
        };
        flowbox.set_parent (this);

        for (int i = 0; i < 12; i++) {
            var image = new Gtk.Image.from_icon_name ("image-missing-symbolic");
            image.add_css_class ("thumbnail");
            flowbox.append (image);

            if (i % 3 == 0)
                image.add_css_class ("filled");
        }

        var controller = new Gtk.EventControllerMotion ();
        controller.motion.connect ((x, y) => {
            if (pressed_widget == null)
                set_child_flags (x, y, ref hovered_widget, Gtk.StateFlags.PRELIGHT);
        });
        add_controller (controller);

        var gesture = new Gtk.GestureClick ();
        gesture.pressed.connect ((n_press, x, y) => {
            set_child_flags (x, y, ref pressed_widget, Gtk.StateFlags.ACTIVE);
        });
        gesture.released.connect ((n_press, x, y) => {
            unset_child_flags (ref pressed_widget, Gtk.StateFlags.ACTIVE);
        });
        add_controller (gesture);
    }

    private void set_child_flags (double x, double y, ref Gtk.Widget? prev_widget, Gtk.StateFlags flags) {
        var widget = pick_corrected (x, y);
        
        if (widget == prev_widget)
            return;

        unset_child_flags (ref prev_widget, flags);

        prev_widget = widget;

        if (prev_widget != null) {
            var w = prev_widget;

            while (w != this) {
                w.set_state_flags (flags, false);
                w = w.get_parent ();
            }
        }
    }

    private void unset_child_flags (ref Gtk.Widget? widget, Gtk.StateFlags flags) {
        if (widget == null)
            return;

        var w = widget;

        while (w != this) {
            w.unset_state_flags (flags);
            w = w.get_parent ();
        }
        
        widget = null;
    }

    private Gtk.Widget? pick_corrected (double x, double y) {
        double shrink_amount = Math.sin (x * Math.PI / get_width ()) * SHRINK_AMOUNT * (SHRINK_CENTER - y / get_height ());

        y -= shrink_amount * get_height ();

        var widget = pick (x, y, Gtk.PickFlags.NON_TARGETABLE);

        if (widget == this)
            return null;

        return widget;
    }

    static construct {
        set_layout_manager_type (typeof (Gtk.BinLayout));
    }

    protected override void dispose () {
        if (flowbox != null) {
            flowbox.unparent ();
            flowbox = null;
        }
        
        base.dispose ();
    }

    private void ensure_shader (string shader_text, ref Gsk.GLShader? shader, ref bool shader_compiled) {
        if (shader != null)
            return;

        var bytes = new Bytes.static ((uint8[]) shader_text);
        shader = new Gsk.GLShader.from_bytes (bytes);

        var renderer = get_native ().get_renderer ();

        try {
            shader_compiled = shader.compile (renderer);
        }
        catch (Error e) {
            critical (e.message);
            /* We don't really care about errors here */
            shader_compiled = false;
        }
    }

    private void snapshot_grid (Gtk.Snapshot snapshot) {
        var reflection_snapshot = new Gtk.Snapshot ();

        if (reflection_shader_compiled) {
            Graphene.Rect bounds = {};
            bounds.init (0, 0, get_width (), get_height ());

            float y = (float) get_height () - flowbox.margin_bottom - 4;

            reflection_snapshot.push_gl_shader (
                reflection_shader,
                bounds,
                reflection_shader.format_args (reflectionY: y)
            );
        }

        snapshot_child (flowbox, reflection_snapshot);

        if (reflection_shader_compiled) {
            reflection_snapshot.gl_shader_pop_texture ();
            reflection_snapshot.pop ();
        }

        var node = reflection_snapshot.free_to_node ();
        snapshot.append_node (node);
    }

    protected override void snapshot (Gtk.Snapshot snapshot) {
        ensure_shader (REFLECTION_SHADER, ref reflection_shader, ref reflection_shader_compiled);
        ensure_shader (CURVATURE_SHADER, ref curvature_shader, ref curvature_shader_compiled);

        if (!reflection_shader_compiled || !curvature_shader_compiled) {
            base.snapshot (snapshot);
            return;
        }

        var curvature_snapshot = new Gtk.Snapshot ();

        if (curvature_shader_compiled) {
            Graphene.Rect bounds = {};
            bounds.init (0, 0, get_width (), get_height ());

            curvature_snapshot.push_gl_shader (
                curvature_shader,
                bounds,
                curvature_shader.format_args ()
            );
        }

        snapshot_grid (curvature_snapshot);

        if (curvature_shader_compiled) {
            curvature_snapshot.gl_shader_pop_texture ();
            curvature_snapshot.pop ();
        }

        var node = curvature_snapshot.free_to_node ();
        snapshot.append_node (node);
    }
}
